use examanalysis_db;
## 1. What is the total number of female students among the students who took all the exams?
select count(student_id) as "Number of female students who took all exams:" from students where students.studentgender="F" and students.student_id in (
select student_id from examsofstudentsdetails);

## 2. What is the GPA of male students who take a given exam?


## 3. What is the minimum grade on a given exam?


## 4. What is the interval length (max-min) of a given exam?


## 5. What is the arithmetic mean of a given exam?


## 6. What is the standard deviation of a given test?


## 7. What is the median of a given exam?


## 8. What is the most repeated (mod) grade on a given exam?


## 9. What is the percentage of those who took and did not take the exam in a given group (for example, all 10th graders)?


## 10. What is a list of students in a given exam?
select student_classes.class_name, students.studentname, students.studentsurname 
from  students join student_classes on students.class_id=student_classes.class_id
where student_classes.class_name="9-B" order by students.studentname;

## 11. What is the total number of students in the classes?
select student_classes.class_name as "Class Name", count(students.class_id) as "Number of students"
from student_classes join students on student_classes.class_id = students.class_id 
group by student_classes.class_name
order by student_classes.class_name;

## 12. Who is the eldest of the students who took the exam?
select min(students.studentbirthday) 
from examsofstudentsdetails join students on examsofstudentsdetails.student_id=students.student_id;

## 13. What is the list of all students who took the exam on a given date?
select students.student_id ,students.studentname, students.studentsurname, exams.examdate 
from examsofstudentsdetails join exams join students 
on examsofstudentsdetails.exam_id=exams.exam_id and students.student_id=examsofstudentsdetails.student_id
where exams.examdate="2020-4-4"
order by students.studentname;